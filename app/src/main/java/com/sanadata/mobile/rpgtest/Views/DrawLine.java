package com.sanadata.mobile.rpgtest.Views;

/**
 * Created by iman on 5/14/19.
 */

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;

public class DrawLine extends View {
    Paint paint = new Paint();


    private PointF initPoint ;
    private PointF endPoint;


    private void init() {
        paint.setColor(Color.WHITE);
        paint.setStrokeWidth(10);
        paint.setPathEffect(new DashPathEffect(new float[] {10,20}, 0));
    }

    public DrawLine(Context context) {
        super(context);
        init();
    }

    public DrawLine(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DrawLine(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }


    public void setStart(float x,float y){
        initPoint  = new PointF();
        initPoint.x =  x;
        initPoint.y = y;
    }

    public void move(float x,float y){
        endPoint  = new PointF();
        endPoint.x =  x;
        endPoint.y =  y;
        invalidate();
    }


    public void setTarget(){
        initPoint = null;
        endPoint = null;
        invalidate();
    }


    @Override
    public void onDraw(Canvas canvas) {
        if (initPoint != null && endPoint!= null ) {
            canvas.drawLine(initPoint.x,initPoint.y, endPoint.x,endPoint.y, paint);
        }

    }

}