package com.sanadata.mobile.rpgtest.Utiles;

import android.app.Activity;
import android.graphics.Point;
import android.util.DisplayMetrics;

/**
 * Created by iman on 5/16/19.
 */

public class ScreenSize {


    public Point screenSize(Activity activity){

        Point point = new Point();

        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        point.y = displayMetrics.heightPixels;
        point.x = displayMetrics.widthPixels;

        return point;
    }




}
