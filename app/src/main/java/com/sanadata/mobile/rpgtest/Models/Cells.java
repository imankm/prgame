package com.sanadata.mobile.rpgtest.Models;

import android.app.Activity;
import android.graphics.Point;
import android.util.DisplayMetrics;
import android.view.View;

import com.sanadata.mobile.rpgtest.R;

/**
 * Created by iman on 5/16/19.
 */

public class Cells {

    Cell[][] cells = new Cell[5][10];
    SoldierModel[] soldierModels;


    public Cells(Activity activity, SoldierModel[] soldierModels) {

        this.soldierModels = soldierModels;

        for(int row = 0;row<5;row++){
            for(int column = 0;column<10;column++){
                cells[row][column] = new Cell();
            }
        }


        cells[0][0].soldieImage = activity.findViewById(R.id.img_soldie_00);
        cells[0][1].soldieImage = activity.findViewById(R.id.img_soldie_01);
        cells[0][2].soldieImage = activity.findViewById(R.id.img_soldie_02);
        cells[0][3].soldieImage = activity.findViewById(R.id.img_soldie_03);
        cells[0][4].soldieImage = activity.findViewById(R.id.img_soldie_04);
        cells[0][5].soldieImage = activity.findViewById(R.id.img_soldie_05);
        cells[0][6].soldieImage = activity.findViewById(R.id.img_soldie_06);
        cells[0][7].soldieImage = activity.findViewById(R.id.img_soldie_07);
        cells[0][8].soldieImage = activity.findViewById(R.id.img_soldie_08);
        cells[0][9].soldieImage = activity.findViewById(R.id.img_soldie_09);
        cells[1][0].soldieImage = activity.findViewById(R.id.img_soldie_10);
        cells[1][1].soldieImage = activity.findViewById(R.id.img_soldie_11);
        cells[1][2].soldieImage = activity.findViewById(R.id.img_soldie_12);
        cells[1][3].soldieImage = activity.findViewById(R.id.img_soldie_13);
        cells[1][4].soldieImage = activity.findViewById(R.id.img_soldie_14);
        cells[1][5].soldieImage = activity.findViewById(R.id.img_soldie_15);
        cells[1][6].soldieImage = activity.findViewById(R.id.img_soldie_16);
        cells[1][7].soldieImage = activity.findViewById(R.id.img_soldie_17);
        cells[1][8].soldieImage = activity.findViewById(R.id.img_soldie_18);
        cells[1][9].soldieImage = activity.findViewById(R.id.img_soldie_19);
        cells[2][0].soldieImage = activity.findViewById(R.id.img_soldie_20);
        cells[2][1].soldieImage = activity.findViewById(R.id.img_soldie_21);
        cells[2][2].soldieImage = activity.findViewById(R.id.img_soldie_22);
        cells[2][3].soldieImage = activity.findViewById(R.id.img_soldie_23);
        cells[2][4].soldieImage = activity.findViewById(R.id.img_soldie_24);
        cells[2][5].soldieImage = activity.findViewById(R.id.img_soldie_25);
        cells[2][6].soldieImage = activity.findViewById(R.id.img_soldie_26);
        cells[2][7].soldieImage = activity.findViewById(R.id.img_soldie_27);
        cells[2][8].soldieImage = activity.findViewById(R.id.img_soldie_28);
        cells[2][9].soldieImage = activity.findViewById(R.id.img_soldie_29);
        cells[3][0].soldieImage = activity.findViewById(R.id.img_soldie_30);
        cells[3][1].soldieImage = activity.findViewById(R.id.img_soldie_31);
        cells[3][2].soldieImage = activity.findViewById(R.id.img_soldie_32);
        cells[3][3].soldieImage = activity.findViewById(R.id.img_soldie_33);
        cells[3][4].soldieImage = activity.findViewById(R.id.img_soldie_34);
        cells[3][5].soldieImage = activity.findViewById(R.id.img_soldie_35);
        cells[3][6].soldieImage = activity.findViewById(R.id.img_soldie_36);
        cells[3][7].soldieImage = activity.findViewById(R.id.img_soldie_37);
        cells[3][8].soldieImage = activity.findViewById(R.id.img_soldie_38);
        cells[3][9].soldieImage = activity.findViewById(R.id.img_soldie_39);
        cells[4][0].soldieImage = activity.findViewById(R.id.img_soldie_40);
        cells[4][1].soldieImage = activity.findViewById(R.id.img_soldie_41);
        cells[4][2].soldieImage = activity.findViewById(R.id.img_soldie_42);
        cells[4][3].soldieImage = activity.findViewById(R.id.img_soldie_43);
        cells[4][4].soldieImage = activity.findViewById(R.id.img_soldie_44);
        cells[4][5].soldieImage = activity.findViewById(R.id.img_soldie_45);
        cells[4][6].soldieImage = activity.findViewById(R.id.img_soldie_46);
        cells[4][7].soldieImage = activity.findViewById(R.id.img_soldie_47);
        cells[4][8].soldieImage = activity.findViewById(R.id.img_soldie_48);
        cells[4][9].soldieImage = activity.findViewById(R.id.img_soldie_49);



        cells[0][0].animationImage = activity.findViewById(R.id.img_fier_00);
        cells[0][1].animationImage = activity.findViewById(R.id.img_fier_01);
        cells[0][2].animationImage = activity.findViewById(R.id.img_fier_02);
        cells[0][3].animationImage = activity.findViewById(R.id.img_fier_03);
        cells[0][4].animationImage = activity.findViewById(R.id.img_fier_04);
        cells[0][5].animationImage = activity.findViewById(R.id.img_fier_05);
        cells[0][6].animationImage = activity.findViewById(R.id.img_fier_06);
        cells[0][7].animationImage = activity.findViewById(R.id.img_fier_07);
        cells[0][8].animationImage = activity.findViewById(R.id.img_fier_08);
        cells[0][9].animationImage = activity.findViewById(R.id.img_fier_09);
        cells[1][0].animationImage = activity.findViewById(R.id.img_fier_10);
        cells[1][1].animationImage = activity.findViewById(R.id.img_fier_11);
        cells[1][2].animationImage = activity.findViewById(R.id.img_fier_12);
        cells[1][3].animationImage = activity.findViewById(R.id.img_fier_13);
        cells[1][4].animationImage = activity.findViewById(R.id.img_fier_14);
        cells[1][5].animationImage = activity.findViewById(R.id.img_fier_15);
        cells[1][6].animationImage = activity.findViewById(R.id.img_fier_16);
        cells[1][7].animationImage = activity.findViewById(R.id.img_fier_17);
        cells[1][8].animationImage = activity.findViewById(R.id.img_fier_18);
        cells[1][9].animationImage = activity.findViewById(R.id.img_fier_19);
        cells[2][0].animationImage = activity.findViewById(R.id.img_fier_20);
        cells[2][1].animationImage = activity.findViewById(R.id.img_fier_21);
        cells[2][2].animationImage = activity.findViewById(R.id.img_fier_22);
        cells[2][3].animationImage = activity.findViewById(R.id.img_fier_23);
        cells[2][4].animationImage = activity.findViewById(R.id.img_fier_24);
        cells[2][5].animationImage = activity.findViewById(R.id.img_fier_25);
        cells[2][6].animationImage = activity.findViewById(R.id.img_fier_26);
        cells[2][7].animationImage = activity.findViewById(R.id.img_fier_27);
        cells[2][8].animationImage = activity.findViewById(R.id.img_fier_28);
        cells[2][9].animationImage = activity.findViewById(R.id.img_fier_29);
        cells[3][0].animationImage = activity.findViewById(R.id.img_fier_30);
        cells[3][1].animationImage = activity.findViewById(R.id.img_fier_31);
        cells[3][2].animationImage = activity.findViewById(R.id.img_fier_32);
        cells[3][3].animationImage = activity.findViewById(R.id.img_fier_33);
        cells[3][4].animationImage = activity.findViewById(R.id.img_fier_34);
        cells[3][5].animationImage = activity.findViewById(R.id.img_fier_35);
        cells[3][6].animationImage = activity.findViewById(R.id.img_fier_36);
        cells[3][7].animationImage = activity.findViewById(R.id.img_fier_37);
        cells[3][8].animationImage = activity.findViewById(R.id.img_fier_38);
        cells[3][9].animationImage = activity.findViewById(R.id.img_fier_39);
        cells[4][0].animationImage = activity.findViewById(R.id.img_fier_40);
        cells[4][1].animationImage = activity.findViewById(R.id.img_fier_41);
        cells[4][2].animationImage = activity.findViewById(R.id.img_fier_42);
        cells[4][3].animationImage = activity.findViewById(R.id.img_fier_43);
        cells[4][4].animationImage = activity.findViewById(R.id.img_fier_44);
        cells[4][5].animationImage = activity.findViewById(R.id.img_fier_45);
        cells[4][6].animationImage = activity.findViewById(R.id.img_fier_46);
        cells[4][7].animationImage = activity.findViewById(R.id.img_fier_47);
        cells[4][8].animationImage = activity.findViewById(R.id.img_fier_48);
        cells[4][9].animationImage = activity.findViewById(R.id.img_fier_49);


        cells[0][0].progressBar = activity.findViewById(R.id.prog_00);
        cells[0][1].progressBar = activity.findViewById(R.id.prog_01);
        cells[0][2].progressBar = activity.findViewById(R.id.prog_02);
        cells[0][3].progressBar = activity.findViewById(R.id.prog_03);
        cells[0][4].progressBar = activity.findViewById(R.id.prog_04);
        cells[0][5].progressBar = activity.findViewById(R.id.prog_05);
        cells[0][6].progressBar = activity.findViewById(R.id.prog_06);
        cells[0][7].progressBar = activity.findViewById(R.id.prog_07);
        cells[0][8].progressBar = activity.findViewById(R.id.prog_08);
        cells[0][9].progressBar = activity.findViewById(R.id.prog_09);
        cells[1][0].progressBar = activity.findViewById(R.id.prog_10);
        cells[1][1].progressBar = activity.findViewById(R.id.prog_11);
        cells[1][2].progressBar = activity.findViewById(R.id.prog_12);
        cells[1][3].progressBar = activity.findViewById(R.id.prog_13);
        cells[1][4].progressBar = activity.findViewById(R.id.prog_14);
        cells[1][5].progressBar = activity.findViewById(R.id.prog_15);
        cells[1][6].progressBar = activity.findViewById(R.id.prog_16);
        cells[1][7].progressBar = activity.findViewById(R.id.prog_17);
        cells[1][8].progressBar = activity.findViewById(R.id.prog_18);
        cells[1][9].progressBar = activity.findViewById(R.id.prog_19);
        cells[2][0].progressBar = activity.findViewById(R.id.prog_20);
        cells[2][1].progressBar = activity.findViewById(R.id.prog_21);
        cells[2][2].progressBar = activity.findViewById(R.id.prog_22);
        cells[2][3].progressBar = activity.findViewById(R.id.prog_23);
        cells[2][4].progressBar = activity.findViewById(R.id.prog_24);
        cells[2][5].progressBar = activity.findViewById(R.id.prog_25);
        cells[2][6].progressBar = activity.findViewById(R.id.prog_26);
        cells[2][7].progressBar = activity.findViewById(R.id.prog_27);
        cells[2][8].progressBar = activity.findViewById(R.id.prog_28);
        cells[2][9].progressBar = activity.findViewById(R.id.prog_29);
        cells[3][0].progressBar = activity.findViewById(R.id.prog_30);
        cells[3][1].progressBar = activity.findViewById(R.id.prog_31);
        cells[3][2].progressBar = activity.findViewById(R.id.prog_32);
        cells[3][3].progressBar = activity.findViewById(R.id.prog_33);
        cells[3][4].progressBar = activity.findViewById(R.id.prog_34);
        cells[3][5].progressBar = activity.findViewById(R.id.prog_35);
        cells[3][6].progressBar = activity.findViewById(R.id.prog_36);
        cells[3][7].progressBar = activity.findViewById(R.id.prog_37);
        cells[3][8].progressBar = activity.findViewById(R.id.prog_38);
        cells[3][9].progressBar = activity.findViewById(R.id.prog_39);
        cells[4][0].progressBar = activity.findViewById(R.id.prog_40);
        cells[4][1].progressBar = activity.findViewById(R.id.prog_41);
        cells[4][2].progressBar = activity.findViewById(R.id.prog_42);
        cells[4][3].progressBar = activity.findViewById(R.id.prog_43);
        cells[4][4].progressBar = activity.findViewById(R.id.prog_44);
        cells[4][5].progressBar = activity.findViewById(R.id.prog_45);
        cells[4][6].progressBar = activity.findViewById(R.id.prog_46);
        cells[4][7].progressBar = activity.findViewById(R.id.prog_47);
        cells[4][8].progressBar = activity.findViewById(R.id.prog_48);
        cells[4][9].progressBar = activity.findViewById(R.id.prog_49);






        for(int row = 0;row<5;row++){
            for(int column = 0;column<10;column++){
                setPosition(row, column);
            }
        }

        for (SoldierModel s:soldierModels) {

            cells[s.getxPosition()][s.getyPosition()].soldieImage.setImageResource(s.getImage());
            cells[s.getxPosition()][s.getyPosition()].soldieImage.setVisibility(View.VISIBLE);
            cells[s.getxPosition()][s.getyPosition()].soldierModel = s;
            cells[s.getxPosition()][s.getyPosition()].cellStatus = CellStatus.FULL;
            cells[s.getxPosition()][s.getyPosition()].progressBar.setVisibility(View.VISIBLE);
            cells[s.getxPosition()][s.getyPosition()].progressBar.setProgress(100);
        }

    }





    public WinStatus getWinStatus(){

        boolean bFriends = false;
        for (SoldierModel s:soldierModels) {
           if(s.getSoldierType() == SoldierType.FRIEND && s.getCurrentHealth()>0){
               bFriends=true;
           }
        }

        if(!bFriends){
            return WinStatus.ENEMY;
        }



        boolean bEnemy = false;
        for (SoldierModel s:soldierModels) {
            if(s.getSoldierType() == SoldierType.ENEMY && s.getCurrentHealth()>0){
                bEnemy=true;
            }
        }

        if(!bEnemy){
            return WinStatus.FRIENDS;
        }


        return  WinStatus.NONE;
    }





    public int getFriendsTurnNumber(){
        int temp = 0;
        for (SoldierModel s:soldierModels) {
            if (s.getSoldierType() == SoldierType.FRIEND && s.getCurrentHealth()>0) {
                temp+=1;
            }
        }
        return temp;
    }



    public int getEnemyTurnNumber(){
        int temp = 0;
        for (SoldierModel s:soldierModels) {
            if (s.getSoldierType() == SoldierType.ENEMY && s.getCurrentHealth()>0) {
                temp+=1;
            }
        }
        return temp;
    }






    public void setFriendReady(){
        for (SoldierModel s:soldierModels) {
            if (s.getSoldierType() == SoldierType.FRIEND) {
                s.setFightStatus(FightStatus.READY);
            }
        }
    }



    public void setEnemyReady(){
        for (SoldierModel s:soldierModels) {
            if (s.getSoldierType() == SoldierType.ENEMY) {
                s.setFightStatus(FightStatus.READY);
            }
        }
    }




    private void setPosition(int row,int colomn){
        cells[row][colomn].setPosition(new Point(row,colomn));
    }



    public Cell getEnemyByNumber(){
        for(int row = 0;row<5;row++){
            for(int column = 0;column<10;column++){
               if(cells[row][column].soldierModel != null && cells[row][column].soldierModel.getSoldierType() == SoldierType.ENEMY && cells[row][column].soldierModel.getFightStatus() == FightStatus.READY
                       && cells[row][column].soldierModel.getCurrentHealth()>0){
                       return cells[row][column];

               }
            }
        }
        return null;
    }



    public Cell getFriendByNumber(){

        for(int row = 0;row<5;row++){
            for(int column = 0;column<10;column++){
                if(cells[row][column].soldierModel != null &&  cells[row][column].soldierModel.getSoldierType() == SoldierType.FRIEND
                        && cells[row][column].soldierModel.getCurrentHealth()>0){
                        return cells[row][column];

                }
            }
        }
        return null;
    }





    public Cell cell(Point point){
       return cells[point.x][point.y];
    }



}
