package com.sanadata.mobile.rpgtest.Models;

import com.sanadata.mobile.rpgtest.Weapon.Weapon;

/**
 * Created by iman on 5/16/19.
 */






public class SoldierModel {

    private SoldierCategory soldierCategory;

    private int maxHealth = 0;

    private int currentHealth = 0;

    private SoldierType soldierType;

    private int xPosition = 0;

    private int yPosition = 0;

    private Weapon weapon;

    private int image;

    private  FightStatus fightStatus = FightStatus.FINISHED;


    public FightStatus getFightStatus() {
        return fightStatus;
    }

    public void setFightStatus(FightStatus fightStatus) {
        this.fightStatus = fightStatus;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public SoldierCategory getSoldierCategory() {
        return soldierCategory;
    }

    public void setSoldierCategory(SoldierCategory soldierCategory) {
        this.soldierCategory = soldierCategory;
    }

    public SoldierType getSoldierType() {
        return soldierType;
    }

    public void setSoldierType(SoldierType soldierType) {
        this.soldierType = soldierType;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }





    public int getMaxHealth() {
        return maxHealth;
    }

    public void setMaxHealth(int maxHealth) {
        this.maxHealth = maxHealth;
    }

    public int getCurrentHealth() {
        return currentHealth;
    }

    public void setCurrentHealth(int currentHealth) {
        this.currentHealth = currentHealth;
    }



    public int getxPosition() {
        return xPosition;
    }

    public void setxPosition(int xPosition) {
        this.xPosition = xPosition;
    }

    public int getyPosition() {
        return yPosition;
    }

    public void setyPosition(int yPosition) {
        this.yPosition = yPosition;
    }
}
