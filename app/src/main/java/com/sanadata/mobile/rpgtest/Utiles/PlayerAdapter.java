package com.sanadata.mobile.rpgtest.Utiles;

import android.content.Context;
import android.media.MediaPlayer;

import com.sanadata.mobile.rpgtest.R;

/**
 * Created by iman on 5/16/19.
 */

public class PlayerAdapter {

    static MediaPlayer mediaPlayerBackGround;
    static MediaPlayer mediaPlayerFire;
    static MediaPlayer mediaPlayerActionFire;


    public static void backGrounMusic(Context context){
        mediaPlayerBackGround = MediaPlayer.create(context, R.raw.thememelody);
        mediaPlayerBackGround.setVolume(0.5f,0.5f);
        mediaPlayerBackGround.start();
    }

    public static void FireSound(Context context,int name){
        mediaPlayerFire = MediaPlayer.create(context, name);
        mediaPlayerFire.start();
    }

    public static void FireActionSound(Context context){
        mediaPlayerActionFire = MediaPlayer.create(context, R.raw.exps1);
        mediaPlayerActionFire.start();
    }



}
