package com.sanadata.mobile.rpgtest.Models;

import android.app.Activity;

import com.sanadata.mobile.rpgtest.R;
import com.sanadata.mobile.rpgtest.Views.DrawFire;
import com.sanadata.mobile.rpgtest.Weapon.AK47;
import com.sanadata.mobile.rpgtest.Weapon.Bazooka;
import com.sanadata.mobile.rpgtest.Weapon.Sniper;
import com.sanadata.mobile.rpgtest.Weapon.Weapon;

/**
 * Created by iman on 5/16/19.
 */

public class StatesSoldiers {


   public static SoldierModel[] stateOne(Activity activity, DrawFire drawFire) {


        SoldierModel[] soldierModels = new SoldierModel[6];

        SoldierModel soldierModel1  = new SoldierModel();
        soldierModel1.setCurrentHealth(250);
        soldierModel1.setMaxHealth(250);
        soldierModel1.setSoldierType(SoldierType.FRIEND);
        soldierModel1.setFightStatus(FightStatus.READY);
        soldierModel1.setxPosition(4);
        soldierModel1.setyPosition(9);
        soldierModel1.setImage(R.drawable.s1);
        soldierModel1.setSoldierCategory(SoldierCategory.BOMBER);
        Weapon weapon1 = new Bazooka(activity,drawFire);
        weapon1.setAccuracy(100);
        weapon1.setHit(100);
        soldierModel1.setWeapon(weapon1);

        soldierModels[0] = soldierModel1;

        SoldierModel soldierModel2  = new SoldierModel();
        soldierModel2.setCurrentHealth(350);
        soldierModel2.setMaxHealth(350);
        soldierModel2.setSoldierType(SoldierType.FRIEND);
        soldierModel2.setxPosition(2);
        soldierModel2.setyPosition(9);
        soldierModel2.setFightStatus(FightStatus.READY);
        soldierModel2.setImage(R.drawable.s2);
        soldierModel2.setSoldierCategory(SoldierCategory.COMMANDER);

        Weapon weapon2 = new AK47(activity,drawFire);
        weapon2.setAccuracy(100);
        weapon2.setHit(80);
        soldierModel2.setWeapon(weapon2);

        soldierModels[1] = soldierModel2;


       SoldierModel soldierModel3  = new SoldierModel();
        soldierModel3.setCurrentHealth(290);
        soldierModel3.setMaxHealth(290);
        soldierModel3.setSoldierType(SoldierType.FRIEND);
        soldierModel3.setFightStatus(FightStatus.READY);
        soldierModel3.setxPosition(0);
        soldierModel3.setyPosition(9);
        soldierModel3.setImage(R.drawable.s3);
        soldierModel3.setSoldierCategory(SoldierCategory.SNIPER);

        Weapon weapon3 = new Sniper(activity,drawFire);
        weapon3.setAccuracy(100);
        weapon3.setHit(100);
        soldierModel3.setWeapon(weapon3);

        soldierModels[2] = soldierModel3;



        SoldierModel soldierModel4  = new SoldierModel();
        soldierModel4.setCurrentHealth(390);
        soldierModel4.setMaxHealth(390);
        soldierModel4.setSoldierType(SoldierType.ENEMY);
        soldierModel4.setFightStatus(FightStatus.FINISHED);
        soldierModel4.setxPosition(0);
        soldierModel4.setyPosition(0);
        soldierModel4.setImage(R.drawable.s4);
        soldierModel4.setSoldierCategory(SoldierCategory.BOMBER);

        Weapon weapon4 = new AK47(activity,drawFire);
        weapon4.setAccuracy(100);
        weapon4.setHit(100);
        soldierModel4.setWeapon(weapon4);

        soldierModels[3] = soldierModel4;


       SoldierModel soldierModel5  = new SoldierModel();
        soldierModel5.setCurrentHealth(280);
        soldierModel5.setMaxHealth(280);
        soldierModel5.setSoldierType(SoldierType.ENEMY);
       soldierModel5.setFightStatus(FightStatus.FINISHED);
        soldierModel5.setxPosition(1);
        soldierModel5.setyPosition(0);
       soldierModel5.setImage(R.drawable.s5);
        soldierModel5.setSoldierCategory(SoldierCategory.COMMANDER);

       Weapon weapon5 = new AK47(activity,drawFire);
       weapon5.setAccuracy(100);
       weapon5.setHit(100);
       soldierModel5.setWeapon(weapon5);

        soldierModels[4] = soldierModel5;

        SoldierModel soldierModel6  = new SoldierModel();
        soldierModel6.setCurrentHealth(250);
        soldierModel6.setMaxHealth(250);
        soldierModel6.setSoldierType(SoldierType.ENEMY);
        soldierModel6.setFightStatus(FightStatus.FINISHED);
        soldierModel6.setxPosition(2);
        soldierModel6.setyPosition(0);
        soldierModel6.setImage(R.drawable.s7);
        soldierModel6.setSoldierCategory(SoldierCategory.COMMANDER);

       Weapon weapon6 = new AK47(activity,drawFire);
       weapon6.setAccuracy(100);
       weapon6.setHit(100);
       soldierModel6.setWeapon(weapon6);

        soldierModels[5] = soldierModel6;


        return  soldierModels;
    }


}
