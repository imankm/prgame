package com.sanadata.mobile.rpgtest.Weapon;

import android.app.Activity;

import com.sanadata.mobile.rpgtest.R;
import com.sanadata.mobile.rpgtest.Utiles.PlayerAdapter;
import com.sanadata.mobile.rpgtest.Views.DrawFire;

/**
 * Created by iman on 5/17/19.
 */

public abstract class Weapon implements WeaponInterface {

    private int hit = 0;
    private int accuracy = 0;
    protected DrawFire drawFire = null;
    protected Activity activity;

    public Weapon(Activity activity, DrawFire drawFire) {
        this.activity = activity;
        this.drawFire = drawFire;
    }

    protected void PlaySound(int sound){
        PlayerAdapter.FireSound(activity, sound);
    }

    public int getHit() {
        return hit;
    }

    public void setHit(int hit) {
        this.hit = hit;
    }

    public int getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(int accuracy) {
        this.accuracy = accuracy;
    }

}
