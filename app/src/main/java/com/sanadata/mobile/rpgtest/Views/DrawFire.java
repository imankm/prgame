package com.sanadata.mobile.rpgtest.Views;

import android.content.Context;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.sanadata.mobile.rpgtest.Interfaces.EndFire;
import com.sanadata.mobile.rpgtest.Models.TwoPoint;
import com.sanadata.mobile.rpgtest.R;
import com.sanadata.mobile.rpgtest.Weapon.AK47;
import com.sanadata.mobile.rpgtest.Weapon.Bazooka;
import com.sanadata.mobile.rpgtest.Weapon.Weapon;

/**
 * Created by iman on 5/15/19.
 */

public class DrawFire extends RelativeLayout {

    Paint paint = new Paint();

    EndFire endFire;

    private void init() {
        paint.setColor(Color.WHITE);
        paint.setStrokeWidth(10);
        paint.setPathEffect(new DashPathEffect(new float[] {10,20}, 0));
    }

    public DrawFire(Context context) {
        super(context);
        init();
    }

    public DrawFire(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DrawFire(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public void Fire(Weapon myWep, final TwoPoint twoPoint, EndFire endFire){
        this.endFire = endFire;
        if (myWep instanceof AK47){
            bulletFire(twoPoint);
        }else if (myWep instanceof Bazooka){
            rocketFire(twoPoint);
        }else{
            sniperFire(twoPoint);
        }
    }

    private void sniperFire(final TwoPoint twoPoint){

        final ImageView imageView = new ImageView(getContext());
        imageView.setImageDrawable(getResources().getDrawable(R.drawable.bulletfier));
        imageView.setRotation(getAngle(twoPoint));

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(40, 40);
        params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
        params.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);

        params.topMargin =twoPoint.pointAY;
        params.leftMargin =twoPoint.pointAX;

        this.addView(imageView,params);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                translate(imageView,twoPoint.pointBX-twoPoint.pointAX,twoPoint.pointBY-twoPoint.pointAY,200,0);
            }
        }, 50);

    }

    private void bulletFire(final TwoPoint twoPoint){

        final ImageView imageView = new ImageView(getContext());
        imageView.setImageDrawable(getResources().getDrawable(R.drawable.bulletfier));
        imageView.setRotation(getAngle(twoPoint));

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(40, 40);
        params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
        params.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);

        params.topMargin =twoPoint.pointAY;
        params.leftMargin =twoPoint.pointAX;

        this.addView(imageView,params);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                translate(imageView,twoPoint.pointBX-twoPoint.pointAX,twoPoint.pointBY-twoPoint.pointAY,300,2);
            }
        }, 50);

    }

    private void rocketFire(final TwoPoint twoPoint){
        
        final ImageView imageView = new ImageView(getContext());
        imageView.setImageDrawable(getResources().getDrawable(R.drawable.rocket));

        imageView.setRotation(getAngle(twoPoint));

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(50, 50);
        params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
        params.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);

        params.topMargin =twoPoint.pointAY;
        params.leftMargin =twoPoint.pointAX;

        this.addView(imageView,params);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                translate(imageView,twoPoint.pointBX-twoPoint.pointAX,twoPoint.pointBY-twoPoint.pointAY,750,0);
            }
        }, 50);

    }

    public float getAngle(TwoPoint twoPoint) {
        float angle = (float) Math.toDegrees(Math.atan2(twoPoint.pointBY - twoPoint.pointAY, twoPoint.pointBX - twoPoint.pointAX));
        if(angle < 0){
            angle += 360;
        }
        angle += 90;
        return angle;
    }

    private void translate(final View viewToMove, float deltax, float deltay,int duration,int repeat) {

        TranslateAnimation anim = new TranslateAnimation(0, deltax, 0, deltay);
        anim.setDuration(duration);
        anim.setRepeatCount(repeat);

        anim.setAnimationListener(new TranslateAnimation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) { }

            @Override
            public void onAnimationRepeat(Animation animation) { }

            @Override
            public void onAnimationEnd(Animation animation) {
                viewToMove.setVisibility(GONE);
                endFire.end();
            }
        });

        viewToMove.startAnimation(anim);

    }

}
