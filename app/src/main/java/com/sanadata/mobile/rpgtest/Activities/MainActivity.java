package com.sanadata.mobile.rpgtest.Activities;

import android.app.Activity;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.sanadata.mobile.rpgtest.Interfaces.EndFire;
import com.sanadata.mobile.rpgtest.Models.Cell;
import com.sanadata.mobile.rpgtest.Models.CellStatus;
import com.sanadata.mobile.rpgtest.Models.Cells;
import com.sanadata.mobile.rpgtest.Models.FightStatus;
import com.sanadata.mobile.rpgtest.Models.SoldierType;
import com.sanadata.mobile.rpgtest.Models.StatesSoldiers;
import com.sanadata.mobile.rpgtest.Models.TwoPoint;
import com.sanadata.mobile.rpgtest.Models.WinStatus;
import com.sanadata.mobile.rpgtest.R;
import com.sanadata.mobile.rpgtest.Utiles.CellCalculation;
import com.sanadata.mobile.rpgtest.Utiles.PlayerAdapter;
import com.sanadata.mobile.rpgtest.Utiles.ScreenSize;
import com.sanadata.mobile.rpgtest.Views.DrawFire;
import com.sanadata.mobile.rpgtest.Views.DrawLine;


//star for warier  ...> jame abilities
//class for weapon ...> jame abilities
//other type of warier
//use accuracy
//see info of each
//machine strategy
//define speed for gun
//define speed for warier
//create clock for each
//define anti bullet or shield for warier
//define special abilities for each character
//anim charachter
//addable guns





public class MainActivity extends AppCompatActivity implements View.OnTouchListener,EndFire {

    DrawLine drawLine;
    DrawFire drawFire;

    private int cellWidth = 0;
    private int cellHeight = 0;

    private Cells cells;

    private Cell cellStart;
    private Cell cellTarget;

    private CellCalculation cellCalculation ;


    private  int currentRemainTurn = 0;
    private SoldierType turnType = SoldierType.FRIEND;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        drawLine = findViewById(R.id.drawView);
        drawFire = findViewById(R.id.drawFire);

        drawLine.setOnTouchListener(this);

        PlayerAdapter.backGrounMusic(getBaseContext());

        Point point = new ScreenSize().screenSize(this);

        cellHeight = point.y/10;
        cellWidth  = point.x/5;

        cellCalculation = new CellCalculation(cellWidth,cellHeight);

        cells =new Cells(this,StatesSoldiers.stateOne(this,drawFire));

        currentRemainTurn = cells.getFriendsTurnNumber();

    }


    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                 downTouch(motionEvent.getX(),motionEvent.getY());
                 break;
            case MotionEvent.ACTION_MOVE:
                 move(motionEvent.getX(),motionEvent.getY());
                break;
            case MotionEvent.ACTION_UP:
                 upTouch(motionEvent.getX(),motionEvent.getY());
                break;
        }
        return true;
    }


    public void downTouch(float x,float y){
        if(cellStart != null) {
            cellStart.setDeSelect();
        }
        cellStart = getCell(x,y);
        if ( currentRemainTurn > 0 && cellStart.allowBeStart(turnType) ) {
            cellStart.setSelect();
            drawLine.setStart(x,y);
        }
    }

    public void move(float x,float y){
        drawLine.move(x,y);
        if(cellTarget != null && cellStart != null && cellTarget != cellStart  ) {
            cellTarget.setDeSelect();
        }
        cellTarget = getCell(x,y);
        if ( cellTarget.allowBeTarget(turnType) && currentRemainTurn > 0 ) {
             cellTarget.setSelect();
        }
    }

    public void upTouch(float x,float y){
        drawLine.setTarget();
        if ( cellTarget.isSelect() && cellStart.isSelect()) {
            cellStart.getSoldierModel().getWeapon().fireSound();
            cellStart.getSoldierModel().getWeapon().drawFire(actionTwoPoint(),this);
        }
    }

    public void  hit(){
        cellStart.getSoldierModel().getWeapon().actionSound();
        cellStart.getSoldierModel().setFightStatus(FightStatus.FINISHED);
        cellStart.setDeSelect();
        cellTarget.setDeSelect();
        cellTarget.animationImage.setVisibility(View.VISIBLE);
        cellTarget.hit(cellStart.getSoldierModel().getWeapon().getHit());
        cellTarget.StartAnimation(this,cellStart.getSoldierModel().getWeapon());

        afterHitDecision();
    }

    public void afterHitDecision(){
        WinStatus winStatus = cells. getWinStatus();
        if(winStatus != WinStatus.NONE){
           showResult(winStatus);
        }else {
           goNextTurn();
        }
    }

    public void goNextTurn(){
        currentRemainTurn -= 1;
        if (currentRemainTurn <= 0) {
            changeTurn();
        } else if (turnType == SoldierType.ENEMY) {
            runEnemyTurn();
        }
    }

    public void changeTurn(){
        if (turnType == SoldierType.FRIEND) {
            currentRemainTurn = cells.getEnemyTurnNumber();
            turnType = SoldierType.ENEMY;
            cells.setEnemyReady();
            runEnemyTurn();
        } else {
            currentRemainTurn = cells.getFriendsTurnNumber();
            cells.setFriendReady();
            turnType = SoldierType.FRIEND;
        }
    }

    public void showResult(WinStatus winStatus){
        if(winStatus == WinStatus.FRIENDS){
            Toast.makeText(getBaseContext(),"WIN!!!",Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(getBaseContext(),"LOST!!!",Toast.LENGTH_LONG).show();
        }
    }

    public TwoPoint actionTwoPoint(){
        TwoPoint twoPoint = new TwoPoint();
        twoPoint.pointAX = cellStart.getCenterX(cellWidth);
        twoPoint.pointAY = cellStart.getCenterY(cellHeight);
        twoPoint.pointBX = cellTarget.getCenterX(cellWidth);
        twoPoint.pointBY = cellTarget.getCenterY(cellHeight);
        return twoPoint;
    }

    public void runEnemyTurn(){

        final Cell cell = cells.getEnemyByNumber();
        final Cell cell2 = cells.getFriendByNumber();

        final Handler handler1 = new Handler();
        handler1.postDelayed(new Runnable() {
            @Override
            public void run() {
                downTouch(cell.getCenterX(cellWidth),cell.getCenterY(cellHeight));
            }
        }, 400);


        final Handler handler2 = new Handler();
        handler2.postDelayed(new Runnable() {
            @Override
            public void run() {
                move(cell2.getCenterX(cellWidth),cell2.getCenterY(cellHeight));
            }
        }, 1000);


        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                upTouch(cell2.getCenterX(cellWidth),cell2.getCenterY(cellHeight));
            }
        }, 1500);

    }


    private Cell getCell(float x,float y){
        Point point = cellCalculation.getIndex(x,y);
        return cells.cell(point);
    }

    @Override
    public void end() {
        hit();
    }

}
