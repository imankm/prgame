package com.sanadata.mobile.rpgtest.Weapon;

import android.app.Activity;

import com.sanadata.mobile.rpgtest.Interfaces.EndFire;
import com.sanadata.mobile.rpgtest.Models.TwoPoint;
import com.sanadata.mobile.rpgtest.R;
import com.sanadata.mobile.rpgtest.Utiles.PlayerAdapter;
import com.sanadata.mobile.rpgtest.Views.DrawFire;

/**
 * Created by iman on 5/17/19.
 */

public class Bazooka extends Weapon {


    public Bazooka(Activity activity, DrawFire drawFire) {
        super(activity,drawFire);
    }

    @Override
    public void fireSound() {
        super.PlaySound(R.raw.rock1);
    }

    @Override
    public void middleSound() {
        super.PlaySound(R.raw.rock1);
    }

    @Override
    public void actionSound() {
        super.PlaySound(R.raw.rock1);
    }

    @Override
    public void drawFire( TwoPoint twoPoint, EndFire endFire) {
        drawFire.Fire(this,twoPoint,endFire);
    }


}
