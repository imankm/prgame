package com.sanadata.mobile.rpgtest.Models;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.sanadata.mobile.rpgtest.R;
import com.sanadata.mobile.rpgtest.Weapon.Bazooka;
import com.sanadata.mobile.rpgtest.Weapon.Weapon;

/**
 * Created by iman on 5/16/19.
 */

public class Cell {

  public ImageView soldieImage = null;
  public ImageView animationImage = null;
  public ProgressBar progressBar = null;

  public CellStatus cellStatus = CellStatus.EMPTY;

  public SoldierModel soldierModel;


  private boolean isSelect = false;

  private int left = 0;
  private int right = 0;
  private int bottom = 0;
  private int top = 0;


  private int centerX = 0;
  private int centerY = 0;


  private Point position;


  public void StartAnimation(Context context,Weapon weapon){
    if (weapon instanceof Bazooka) {
      animationImage.setImageDrawable(context.getResources().getDrawable(R.drawable.expanim));
      AnimationDrawable animation = (AnimationDrawable) animationImage.getDrawable();
      animation.start();
    }
  }

  public void hit(int hitMount){

    soldierModel.setCurrentHealth(soldierModel.getCurrentHealth()-hitMount);
    progressBar.setProgress((soldierModel.getCurrentHealth()*100)/soldierModel.getMaxHealth());

    if (soldierModel.getCurrentHealth()<=0){
       soldieImage.setVisibility(View.GONE);
       progressBar.setVisibility(View.GONE);
       cellStatus = CellStatus.EMPTY;
    }

  }


  public boolean allowBeStart(SoldierType soldierType){
    if (getSoldierModel().getFightStatus() == FightStatus.READY && getSoldierModel().getSoldierType() == soldierType) {
      return true;
    }
    return false;
  }



  public boolean allowBeTarget(SoldierType soldierType){
    if (cellStatus == CellStatus.FULL && getSoldierModel().getSoldierType() != soldierType) {
      return true;
    }
    return false;
  }

  public boolean isSelect() {
    return isSelect;
  }

  public void setSelect(){
    int color = 0;
    isSelect = true;
    if (soldierModel.getSoldierType() == SoldierType.FRIEND ){
      color = Color.GREEN;
    }else{
      color = Color.RED;
    }
    Drawable drawable = soldieImage.getDrawable();
    drawable.setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.MULTIPLY));
  }

  public void setDeSelect(){
    isSelect = false;
    Drawable drawable = soldieImage.getDrawable();
    if (drawable!= null) {
      drawable.clearColorFilter();
    }
  }

  public SoldierModel getSoldierModel() {
    return soldierModel;
  }

  public void setSoldierModel(SoldierModel soldierModel) {
    this.soldierModel = soldierModel;
  }

  public Point getPosition() {
    return position;
  }

  public void setPosition(Point position) {
    this.position = position;
  }

  public int getLeft(int width) {
    return left;
  }

  public void setLeft(int left) {
    this.left = left;
  }

  public int getRight() {
    return right;
  }

  public void setRight(int right) {
    this.right = right;
  }

  public int getBottom() {
    return bottom;
  }

  public void setBottom(int bottom) {
    this.bottom = bottom;
  }

  public int getTop() {
    return top;
  }

  public void setTop(int top) {
    this.top = top;
  }

  public int getCenterX(int widthCell) {
    return (position.x*widthCell) + (widthCell/2);
  }

  public void setCenterX(int centerX) {
    this.centerX = centerX;
  }

  public int getCenterY(int height) {
    return (position.y*height) + (height/2);
  }

  public void setCenterY(int centerY) {
    this.centerY = centerY;
  }
}
