package com.sanadata.mobile.rpgtest.Utiles;

import android.graphics.Point;

import com.sanadata.mobile.rpgtest.Models.Cell;

/**
 * Created by iman on 5/16/19.
 */

public class CellCalculation {



    private int cellWidth;
    private int cellHeight;


    public CellCalculation(int cellWidth, int cellHeight) {
        this.cellWidth = cellWidth;
        this.cellHeight = cellHeight;
    }

    public int getCellWidth() {
        return cellWidth;
    }

    public void setCellWidth(int cellWidth) {
        this.cellWidth = cellWidth;
    }

    public int getCellHeight() {
        return cellHeight;
    }

    public void setCellHeight(int cellHeight) {
        this.cellHeight = cellHeight;
    }

    public Point getIndex(float x, float y){
            Point point = new Point();
            point.x = (int) Math.floor(x/cellWidth);
            point.y = (int) Math.floor(y/cellHeight);
            return point;
    }



}
