package com.sanadata.mobile.rpgtest.Weapon;

import android.app.Activity;

import com.sanadata.mobile.rpgtest.Interfaces.EndFire;
import com.sanadata.mobile.rpgtest.Models.TwoPoint;
import com.sanadata.mobile.rpgtest.Views.DrawFire;

/**
 * Created by iman on 5/17/19.
 */

public interface WeaponInterface {


    void fireSound();
    void  middleSound();
    void actionSound();
    void drawFire( final TwoPoint twoPoint, EndFire endFire);


}
