package com.sanadata.mobile.rpgtest.Models;

import android.graphics.Point;

/**
 * Created by iman on 5/16/19.
 */

public class TwoPoint {

    public int pointAX = 0;
    public int pointAY = 0;
    public int pointBX = 0;
    public int pointBY = 0;


    @Override
    public String toString() {
        return "TwoPoint{" +
                "pointAX=" + pointAX +
                ", pointAY=" + pointAY +
                ", pointBX=" + pointBX +
                ", pointBY=" + pointBY +
                '}';
    }
}
